import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles, withTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import logo from './static/logo.png';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const styles = {
    root: {
      flexGrow: 1,
      boxShadow: '1px 2px 10px #888888',
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    colorPrimary: {
        backgroundColor: 'white',
        color: 'black',
        padding: 5,
    },
    ToolOption: {        
        padding: 5,
        paddingLeft: '10%',
        paddingRight: '10%',
    },
    option: {
        paddingRight: 20,
    }
  };
  const imgSize={
      height:'31px',
      width: '300px',
  }
  
  function ButtonAppBar(props) {
    const { classes } = props;
    return (
      <div className={classes.root}>
        <AppBar position="static" color="primary" className={classes.colorPrimary}>
          <Toolbar className={classes.ToolOption}>
          <img src={logo} style={imgSize}/>
          <div className={classes.grow}></div>
          <Button>SHOP</Button>
          <Button>MY ACCOUNT</Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
  
  ButtonAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(ButtonAppBar);