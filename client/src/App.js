import React, { Component } from 'react';
import logo from './logo.svg';
import ButtonAppBar from './header.js';
import SimpleAppBar from './preHeader.js';
import Carousel from './carousel.js';
import Collections from './Collections.js';
import Products from './Products.js';
import Articles from './Articles.js';
import Footer from './Footer.js';
import './App.css';

const styles={
  layout:{
    marginLeft: '10%',
    marginRight: '10%',
  }
}
class App extends Component {
  render() {
    return (
      <div className="App">
        <SimpleAppBar/>
        <ButtonAppBar/>
        <Carousel/>
        <div style={styles.layout}>
        <Collections/>
        <Products/>
        <Articles/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
