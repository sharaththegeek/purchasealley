import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Slider from "react-slick";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import product1 from './static/product-01.jpg';
import product2 from './static/product-02.jpg';
import product3 from './static/product-03.jpg';
import product4 from './static/product-04.jpg';
import product5 from './static/product-05.jpg';
import product6 from './static/product-06.jpg';
import product7 from './static/product-07.jpg';
import product8 from './static/product-09.jpg';

const styles={
    cardSize:{
        height: '200px',
        width: '150px',
        backgroundColor: '#eeeeee',
    },
    card1:{
        
        height: '295px',
        width: '271px',
    },
    card2:{
       
        height: '295px',
        width: '271px',
    },
    card3:{
       
        height: '295px',
        width: '271px',
    },
    card4:{
      
        height: '295px',
        width: '271px',
    },
    card5:{
        height: '295px',
        width: '271px',
    },
    card6:{
      
        height: '295px',
        width: '271px',
    },
    card7:{
       
        height: '295px',
        width: '271px',
    },
    card8:{
        
        height: '295px',
        width: '271px',
    },
    lineThick:{
        padding: 1,
    },
    textLayout:{
        textAlign: 'left',
    },
    productText:{
       color: 'black',
       fontWeight: 'bold',
    },
    priceText:{
       color: '#9e9e9e',
    },
    headLayout:{
        textAlign:'left',
        marginLeft: 15,
    },
    tryon:{
        float:'right',
        marginRight:10,
        color:'#47BAC1',
    },
}
class Products extends Component{
    render(){
        const settings = {
            
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            swipeToSlide: true,
            slidesToScroll: 1,
            autoplay: true,
            speed: 2000,
            autoplaySpeed: 5000,
            cssEase: "linear",
          };
        return(
            <div>
            <Typography variant="h5" gutterBottom style={styles.headLayout}>Featured Products</Typography>
            <Divider variant="middle" style={styles.lineThick}/>
            <br/>
            <Slider {...settings}>
            <div>
                <Card style={styles.card1}>
                <img src={product1} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card2}>
            <img src={product2} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <a href="/index.html"><Button variant="outlined" style={styles.tryon}>Try</Button></a>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card3}>
            <img src={product3} height='295px' width='271px'/>
            </Card>
            <br/>
            <div style={styles.textLayout}>
            <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card4}>
            <img src={product4} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card5}>
                <img src={product5} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card6}>
            <img src={product6} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card7}>
            <img src={product7} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            <div>
            <Card style={styles.card8}>
            <img src={product8} height='295px' width='271px'/>
                </Card>
                <br/>
                <div style={styles.textLayout}>
                <Button variant="outlined" style={styles.tryon}>Try</Button>
                <Typography variant="subtitle1" style={styles.productText}>Product</Typography>
                <Typography variant="subtitle2" style={styles.priceText}>Price</Typography>
                </div>
            </div>
            </Slider>
            <br/>
            <br/>
            </div>
        )
    }
};

export default Products;