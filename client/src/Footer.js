import React, {Component} from 'react';
import Divider from '@material-ui/core/Divider';
import { Typography } from '@material-ui/core';

const styles={
    FooterSize:{
        backgroundColor: '#212121',
        height: '15em',
    },
    greenStyle:{
        marginTop: '2%',
        backgroundColor: '#47BAC1',
        height: '0.5em',
    },
    textLayout:{
        marginTop: 0,
        paddingTop: 40,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'stretch',
        marginLeft: "10%",
        marginRight: "10%",
    },
    headColor:{
        color: '#616161',
    },
    bodyColor: {
        color: '#bdbdbd',
    },
    singleLayout:{
        marginRight: 20,
    },
    empty: {
        flexGrow: 1,
    }
}
class Footer extends Component{
 render(){
     return( 
        <div>
        <div style={styles.greenStyle}></div>
        <div style={styles.FooterSize}>
        <div style={styles.textLayout}>
        <div style={styles.singleLayout}>
            <Typography variant="subtitle1" style={styles.headColor}>ACCESSORIES</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Body Care</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Chambray</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Floral</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Rejuvination</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Shaving</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Toilette</Typography>
        </div>
        <div style={styles.singleLayout}>
            <Typography variant="subtitle1" style={styles.headColor}>BRANDS</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Barbour</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Brioni</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Oliver Spencer</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Belstaff</Typography>
        </div>
        <div style={styles.singleLayout}>
            <Typography variant="subtitle1" style={styles.headColor}>ACCESSORIES</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Body Care</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Chambray</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Floral</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Rejuvination</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Shaving</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Toilette</Typography>
        </div>
        <div style={styles.singleLayout}>
            <Typography variant="subtitle1" style={styles.headColor}>GET IN TOUCH</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Call us at (555)-555</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>support@purchasealley.in</Typography>
        </div>
        <div style={styles.empty}></div>
        <div style={styles.singleLayout}>
            <Typography variant="subtitle1" style={styles.headColor}>NEWSLETTER</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>SIGN UP NOW</Typography>
            <Typography variant="subtitle2" style={styles.bodyColor}>Enter your email address and get notified about new products. We hate spam!</Typography>
        </div>
        </div>
        </div>
        </div> 
     )
 }
};

export default Footer;