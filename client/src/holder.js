import React, {Component} from 'react';
import Grow from '@material-ui/core/Grow';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Backdrop } from '@material-ui/core';

const styles={
    layout: {
        display: "flex",
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center',
    },
    lateBloom: {
        transformOrigin: '0 0 0',
        timeout: 1000,
    },
    price:{
        color: "#795548",
    },
    buttonColor:{
        color: "#47BAC1",
    },
    textCentered:{
        alignSelf: 'center',
    }
}
class Holder extends Component{
    constructor(props){
        super(props);
        this.state={}
    }
    render(){
        var renderItems=""
        if(this.props.text==="left"){
            renderItems=(
                <div style={styles.layout}>
                <Grow in='true'>
                <div style={styles.textCentered}>
                <Typography variant="h2" >{this.props.title}</Typography>
           <Typography variant="h6" style={styles.price}>{this.props.description}</Typography>
           <p>{this.props.small}</p>
           <Button variant="outlined" style={styles.buttonColor}>Buy now</Button>
                </div>
                </Grow>
                <Grow in='true' style={styles.lateBloom}>
                <div>
                    <img src={this.props.imgsrc} height="500px" alt="noimg"/>
                </div>
                </Grow>
                </div>
            );
        }
        else{
          renderItems=( 
          <div style={styles.layout}>
          <Grow in='true'>
          <div>
               <img src={this.props.imgsrc} height="500px" alt="noimg"/>
           </div>
           </Grow>
           <Grow in='true' style={styles.lateBloom}>
           <div style={styles.textCentered}>
           <Typography variant="h2" >{this.props.title}</Typography>
           <Typography variant="h6" style={styles.price}>{this.props.description}</Typography>
           <p>{this.props.small}</p>
           <Button variant="outlined" color="default" style={styles.buttonColor}>Buy now</Button>
          </div>
          </Grow>
          </div>
        );
        }
        return(
            <div>
            {renderItems}
            </div>
        )
    }
};

export default Holder;