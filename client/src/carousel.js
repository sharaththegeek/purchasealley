import React, {Component} from 'react';
import FiberManualRecord from '@material-ui/icons/FiberManualRecord';
import FiberManualRecordOutlined from '@material-ui/icons/FiberManualRecordOutlined';
import Holder from './holder.js';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIos from '@material-ui/icons/ArrowForwardIos';
import shoe from './static/shoe1.png';
import sl1 from './static/sl1.png';
import sl2 from './static/sl2.png';

const styles={
    slider: {
        backgroundColor: '#eeeeee',
        height: 500,
        width: '100%',
    },
    imageCarousel: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    holderWidth: {
        flexGrow: 1,
   }
}
class Carousel extends Component{
    constructor(props){
        super(props);
        this.state={
            currentPage:1
        }
        this.prevPage=this.prevPage.bind(this);
        this.nextPage=this.nextPage.bind(this);
        this.handleClick=this.handleClick.bind(this);
    }
    tick(){
        if(this.state.currentPage==3){
            this.setState({currentPage:1})
        }
        else{
        this.setState({currentPage:this.state.currentPage+1})
        }
    }
    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 10000);
    }
    prevPage(){
        if(this.state.currentPage===1){
            this.setState({currentPage:3})
        }
        else{
        this.setState({currentPage:this.state.currentPage-1})
        }
    }
    nextPage(){
        if(this.state.currentPage===3){
            this.setState({currentPage:1})
        }
        else{
        this.setState({currentPage:this.state.currentPage+1})
        }
    }
    handleClick(event){
        this.setState({currentPage:Number(event.target.id)})
    }
    render(){
     const display=[];
     display[1]=<Holder imgsrc={shoe} text="left" title="Canvas Sneaker" description="Starts from $259" small="Get the best products here"/>;
     display[3]=<Holder imgsrc={sl1} text="left" title="Summer Hat" description="Starts from $499" small="Look stylish this summer with these hats"/>;
     display[2]=<Holder imgsrc={sl2} title="Ladies Backpack" description="Starts from $259" small="Get the best bags here"/>;
     const page=[1, 2, 3];
     const ButtonCollection=page.map((current)=>{
        if(current===this.state.currentPage){
            return(
               <FiberManualRecordOutlined id={current} key={current}
                />
            );
        } 
        else {
            return(
                <FiberManualRecord id={current} key={current}
                onClick={this.handleClick}/>
            );
        }
     }
     );
     const currentRender=display[this.state.currentPage];
     return(
     <div style={styles.slider}>
      <div style={styles.imageCarousel}>
      <div>
          <ArrowBackIos onClick={this.prevPage}/>
      </div>
      <div style={styles.holderWidth}>
        {currentRender}
      </div>
      <div>
      <ArrowForwardIos onClick={this.nextPage}/>
      </div>
      </div>
      <div>
         
      </div>
     </div>
     );
    }
};

export default Carousel;