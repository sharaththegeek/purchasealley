import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import col1 from './static/featured-collection-01.jpg';
import col2 from './static/featured-collection-02.jpg';
import col3 from './static/featured-collection-03.jpg';
import Fade from '@material-ui/core/Fade';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles={
    cardSet:{
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
    },
    card1:{
        backgroundImage: `url(${col1})`,
        height: '350px',
        width: '350px',
        marginRight: '20px',
    },
    card2:{
        backgroundImage: `url(${col2})`,
        height: '350px',
        width: '350px',
        marginRight: '20px',
    },
    card3:{
        backgroundImage: `url(${col3})`,
        height: '350px',
        width: '350px',
        marginRight: '20px',
    },
    textLayout:{
        textAlign:'left',
    },
    lineThick:{
        padding: 1,
        width: '100%',
    },
    headLayout:{
        textAlign:'left',
        marginLeft: 15,
    }
}
class Collections extends Component{
    render(){
        return(
            <div>
            <br/>
            <br/>
            <Typography variant="h5" gutterBottom style={styles.headLayout}>Featured Collections</Typography>
            <Divider variant="middle" style={styles.lineThick}/>
            <br/>
            <div style={styles.cardSet}>
            <Card style={styles.card1}>
                <CardContent style={styles.textLayout}>
                    <Typography variant="h6" gutterBottom>Shoe Collection</Typography>
                    <Typography color="textSecondary">Starting from $59</Typography>
                </CardContent>
            </Card>
            <Card style={styles.card2} >
                <CardContent style={styles.textLayout}>
                    <Typography variant="h6" gutterBottom>Bag Collection</Typography>
                    <Typography color="textSecondary">Starting from $150</Typography>
                </CardContent>
            </Card>
            <Card style={styles.card3}>
                <CardContent style={styles.textLayout}>
                    <Typography variant="h6" gutterBottom>Glasses Collection</Typography>
                    <Typography color="textSecondary">Starting from $25</Typography>
                </CardContent>
            </Card>
            </div>
            <br/>
            <br/>
            </div>
        )
    }
};

export default Collections;