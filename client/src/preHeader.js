import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, withTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import SearchIcon from '@material-ui/icons/Search';
import Shop from '@material-ui/icons/ShoppingCart';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = {
  root: {
    flexGrow: 1,
  },
  preMain: {
     backgroundColor: "#212121",
     height: '2.2em',
  },
  Tools: {
      flexGrow:1,
      margin: -15,
      marginRight: 40,
  },
  LogText: {
      color: '#9e9e9e',
      paddingRight: 10,
  },
  OrText: {
      color: '#616161',
      paddingRight: 10,
  },
  socialIcons:{
      flexGrow:1,
  },
  shopButton:{
     backgroundColor: '#47BAC1',
     color: 'white',
     width: '80px',
  },
  SpacingEqual:{
      paddingRight: 10,
  }
}

function SimpleAppBar(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" color="primary" className={classes.preMain}>
      <Toolbar className={classes.Tools}>
        <div className={classes.socialIcons}></div>
        <Typography className={classes.LogText}>Log in</Typography>
        <Typography className={classes.OrText}>or</Typography>
        <Typography className={classes.LogText}>Create an account</Typography>
        <Typography className={classes.LogText}>|</Typography>
        <SearchIcon className={classes.SpacingEqual}/>
        <Button className={classes.shopButton}>
          <Shop/>
            $0
        </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleAppBar);
