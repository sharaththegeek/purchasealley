import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import article1 from './static/articles-01.jpg';
import article2 from './static/articles-02.jpg';
import article3 from './static/articles-03.jpg';

const styles={
cardSet:{
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'space-around',
},
outerCard:{
    height: '300px',
    width: '400px',
    backgroundImage: `url(${article1})`
},
innerCard:{
    backgroundColor: 'white',
    height: '75px',
    width: '75px',
},
Card1:{
    height: '247px',
    width: '377px',
    backgroundImage: `url(${article1})`,
    marginRight: '20px',
},
Card2:{
    height: '247px',
    width: '377px',
    backgroundImage: `url(${article2})`,
    marginRight: '20px',
},
Card3:{
    height: '247px',
    width: '377px',
    backgroundImage: `url(${article3})`,
    marginRight: '20px',
},
lineThick:{
    padding: 1,
},
NumberText:{
    
    fontSize: '1.5em',
},
MonthText:{
    fontWeight: 'lighter',
},
headLayout:{
    textAlign:'left',
    marginLeft: 15,
},
}
class Articles extends Component{
    render(){
        return(
            <div>
            <Typography variant="h5" gutterBottom style={styles.headLayout}>Latest Articles</Typography>
            <Divider variant="middle" style={styles.lineThick}/>
            <br/>
            <div style={styles.cardSet}>
            <div>
             <Card style={styles.Card1} >
             <CardContent>
                   <Card style={styles.innerCard}>
                     <CardContent>
                         <Typography style={styles.NumberText}>18 </Typography>
                         <Typography style={styles.MonthText}>JAN</Typography>
                     </CardContent>
                   </Card>
                   </CardContent>
                </Card>
             </div><div>
             <Card style={styles.Card2}>
               <CardContent>
                   <Card style={styles.innerCard}>
                     <CardContent>
                         <Typography style={styles.NumberText}>18 </Typography>
                         <Typography style={styles.MonthText}>FEB</Typography>
                     </CardContent>
                   </Card>
               </CardContent>
             </Card>
             </div>
             <div>
             <Card style={styles.Card3}>
               <CardContent>
                   <Card style={styles.innerCard}>
                     <CardContent>
                         <Typography style={styles.NumberText}>18 </Typography>
                         <Typography style={styles.MonthText}>MAR</Typography>
                     </CardContent>
                   </Card>
               </CardContent>
             </Card>
             </div>
            </div>
            <br/>
            <br/>
            </div>
        )
    }
};

export default Articles;